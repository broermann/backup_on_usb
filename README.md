# Backup on USB Devices
Auch ohne Tape Streamer lassen sich Backups auf USB Sticks erstellen. USB Sticks gibt es mit ausreichendem Durchsatz ab USB-3 Standard. Bei täglichen Backups kann je nach Menge der Sticks auf ältere Daten zuückgegriffen werden.

![Sticks](https://www.broermann.com/images/USB-Stick_Backup.png)

Die Sticks werden mit ext4-Filesystem formatiert. Mit **rsync** werden die meisten Blöcke nur einmal geschrieben.

Auf Windowssystem lassen sie die Daten mit dem Programm "ext2explore.exe" wieder herstellen.


#### Label

Die Sticks müssen vorher mit Namen "usbbackup"  gelabelt und formatiert werden


    umount /dev/$USB_DEV
    parted -s /dev/$USB_DEV  mklabel msdos
    parted -s /dev/$USB_DEV  rm 1
    parted  /dev/$USB_DEV  --script -- mkpart primary ext4 2048s 100%
    dd if=/dev/zero of=/dev/${USB_DEV}1 bs=512 count=10
    mkfs.ext4  -L usbbackup  /dev/${USB_DEV}1
    
    
#### Logs


```
# ls -l /var/log/backup/
insgesamt 3404
-rw-r--r-- 1 root root 295303 Feb 19 21:26 backup200219.idx
-rw-r--r-- 1 root root   3898 Feb 20 21:25 backup200220.idx
-rw-r--r-- 1 root root  81111 Feb 21 21:25 backup200221.idx
-rw-r--r-- 1 root root 327203 Feb 24 21:26 backup200224.idx
-rw-r--r-- 1 root root 328059 Feb 25 21:26 backup200225.idx
-rw-r--r-- 1 root root 330578 Feb 26 21:26 backup200226.idx
-rw-r--r-- 1 root root 295072 Feb 27 21:26 backup200227.idx
-rw-r--r-- 1 root root 476625 Feb 28 21:26 backup200228.idx
-rw-r--r-- 1 root root 483339 Mär  2 21:26 backup200302.idx
-rw-r--r-- 1 root root 484262 Mär  3 21:26 backup200303.idx
-rw-r--r-- 1 root root 312286 Mär  4 21:26 backup200304.idx
```
