#!/bin/bash
# 17.7.2017 Bernd Broermann
# 16.8.2017 Mail



DATUM=`date '+%y%m%d'`


. backup_usb.conf




usage () {
       cat <<-EOT
        ${0##*/} -c  # check
        ${0##*/} -u  # umount
        ${0##*/} -b  # backup
	${0##*/} -r  # restore ( not implemented , yet) 
EOT

exit
}

SUDO="sudo"

VERBOSE=false
CHECK=false
BACKUP=false
RESTORE=false
UMOUNT=false

while getopts "vcrub" OPT; do
  case $OPT in
     v) VERBOSE=true ;;
     c) CHECK=true;;
     u) UMOUNT=true ;;
     b) BACKUP=true;;
     r) RESTORE=true;;
     *) usage ;;
     \?)  echo falsche Argumente ;;
  esac

done

backup_mail () {
    local subject=$1
    local body=$2

    echo "From: $BACKUP_MAILFROM" > $BACKUP_MAILFILE
    echo "To: $BACKUP_MAILTO" >> $BACKUP_MAILFILE
    # echo "CC: $MAILCC" >> $BACKUP_MAILFILE
    # echo "Subject: [$BACKUP_HOST] Backup OK" >> $BACKUP_MAILFILE
    echo "Subject: $subject" >> $BACKUP_MAILFILE
    echo >> $BACKUP_MAILFILE
    #echo "Datensicherung $BACKUP_HOST  war erfolgreich !! ; Indexdatei : $BACKUP_IDX " >> $BACKUP_MAILFILE
    echo $body >> $BACKUP_MAILFILE
    cat $BACKUP_MAILFILE | /usr/sbin/sendmail -t
    # rm $BACKUP_MAILFILE
}




check () {
DEVS=$( $SUDO find  /sys/block -type d -o -type l | sed -n "s%/sys/block/\([vxs]d[a-z]*\)$%\1%p" )
for DEV in $DEVS
  do
  UDEVPROPERTY=$( $SUDO udevadm info -q property -n /dev/$DEV | egrep "ID_VENDOR=|ID_MODEL=|ID_SERIAL=|ID_PATH=" )
  echo DEV=$DEV $UDEVPROPERTY

  done > /tmp/udevdiskproperties


  USB_DEV=$(< /tmp/udevdiskproperties  grep VENDOR=${BACKUP_USB_VENDOR} | sed "s/DEV=\(sd[a-z]*\) .*/\1/g")

   if $VERBOSE ; then 
   echo "BACKUP_USB_VENDOR=$BACKUP_USB_VENDOR"
   echo "USB_DEV=$USB_DEV"
   fi  

   if test -z $USB_DEV ; then
	   echo "USB Stick/Disk  [ vendor = $BACKUP_USB_VENDOR ] not found"
	   echo "insert USB Stick"
	   exit 1
   fi   

   ID_FS_LABEL=$( $SUDO udevadm info -q property -n /dev/${USB_DEV}1 | sed -n "s/ID_FS_LABEL=\(.*\)/\1/p" )

   if $VERBOSE ; then
   echo "ID_FS_LABEL=$ID_FS_LABEL"
   fi

   if test "$ID_FS_LABEL" != "$BACKUP_USB_LABEL" ; then 
	        echo "Not labled [ $BACKUP_USB_LABEL ] "

		echo "### ToDo:"
		echo "$SUDO umount \$( df -Ph | sed -n \"s/\(\/dev\/${USB_DEV}[1-9]\) .*/\\\\1/p\") "
		echo "$SUDO parted -s /dev/$USB_DEV  mklabel msdos"
# 		echo "$SUDO parted -s /dev/$USB_DEV  rm 1 "
		echo "$SUDO LANG=C parted  /dev/$USB_DEV  --script -- mkpart primary ext4 2048s 100%"
		echo "$SUDO dd if=/dev/zero of=/dev/${USB_DEV}1 bs=512 count=10"
		echo "$SUDO mkfs.ext4  -L usbbackup  /dev/${USB_DEV}1"
		exit 1 
   fi

RC=0; while test "$RC" = "0"  ; do $SUDO umount $BACKUP_BASE/$BACKUP_USB_LABEL &>/dev/null ; RC=$? ;  done

if test ! -d $BACKUP_BASE/$BACKUP_USB_LABEL ; then mkdir $BACKUP_BASE/$BACKUP_USB_LABEL ; fi

if ! <  /etc/mtab   grep -q "/dev/${USB_DEV}1 $BACKUP_BASE/$BACKUP_USB_LABEL" && < /etc/mtab  grep  -q "/dev/${USB_DEV}1" ; then 
  $SUDO umount /dev/${USB_DEV}1 
fi   


if ! <  /etc/mtab   grep -q "/dev/${USB_DEV}1 $BACKUP_BASE/$BACKUP_USB_LABEL" ; then
  $SUDO mount /dev/${USB_DEV}1 $BACKUP_BASE/$BACKUP_USB_LABEL
fi 
if $VERBOSE ; then  	
   echo "USB Stick from vendor [ $BACKUP_USB_VENDOR ]   with label [ $BACKUP_USB_LABEL ] is mounted on:" 
   $SUDO mount | grep  "/dev/${USB_DEV}1"
fi




MNT_USBBACKUP_DEV=$( $SUDO df -Ph $BACKUP_BASE/$BACKUP_USB_LABEL | sed -n "s/\/dev\/\([a-z0-9]*\) .*/\1/p")

if $VERBOSE ; then
	echo "MNT_USBBACKUP_DEV: $MNT_USBBACKUP_DEV"
	echo "USB_DEV 1: ${USB_DEV}1"
fi
	
if test "$MNT_USBBACKUP_DEV" = "${USB_DEV}1" 
   then     if $VERBOSE ; then echo "backup can start now to $BACKUP_BASE/$BACKUP_USB_LABEL" ; fi
   else     echo "backup cannot start  !!" ; exit 1
fi

}

backup () {

if test -z $BACKUP_SOURCE_EXCLUDE ; then EXCLUDE= ; else EXCLUDE="--exclude=$BACKUP_SOURCE_EXCLUDE" ;fi
# echo "$SUDO rsync -av $RSYNC_OPTION $EXCLUDE $BACKUP_SOURCE $BACKUP_BASE/$BACKUP_USB_LABEL"
$SUDO rsync -av $RSYNC_OPTION $EXCLUDE $BACKUP_SOURCE $BACKUP_BASE/$BACKUP_USB_LABEL > $BACKUP_IDX 2>$BACKUP_ERR


FEHLER=$?


# sed -i -e "/socket ignored/d" -e "/file changed as we read it/d" -e "/tar: Removing leading/d"  $BACKUP_ERR 

if test $(< $BACKUP_ERR wc -l) -eq 0 
   then FEHLER=0 
   else FEHLER=1
fi

if test $FEHLER = "0" ;then
   tail -n1 $BACKUP_ERR >> $BACKUP_IDX
   rm $BACKUP_ERR
   backup_mail "[$BACKUP_HOST] Backup OK" "Datensicherung $BACKUP_HOST  war erfolgreich !! ; Indexdatei : $BACKUP_IDX "

fi

if test $FEHLER -ne "0" ; then
   echo "Bei der Datensicherung $BACKUP_HOST ist ein Fehler aufgetreten"
   echo "Indexdatei : $BACKUP_IDX "
   cat $BACKUP_ERR
   backup_mail "[$BACKUP_HOST] Bei der Datensicherung ist ein Fehler aufgetreten" "$(<$BACKUP_ERR)"
fi

# Lösche alle Logdateien die älter sind als 14 Tage
find $BACKUP_LOGDIR  -mtime +14 -exec rm  {} \;


$SUDO  umount $BACKUP_BASE/$BACKUP_USB_LABEL &>/dev/null
}

restore () {
echo "not implemented"
}


if ! ( $CHECK || $BACKUP || $RESTORE || $UMOUNT )  ; then echo "not one option  is set";  usage  ; fi

if $CHECK ; then check ; fi

if $BACKUP  ; then 
	 check 
	 backup 
fi

if $RESTORE  ; then 
	 check 
	 restore
fi

if $UMOUNT  ; then
         $SUDO  umount $BACKUP_BASE/$BACKUP_USB_LABEL
fi



